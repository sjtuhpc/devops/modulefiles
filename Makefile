.PHONY : all clean git update

all : git update

git :
	git pull origin && git push origin

update :
	rsync -rL --delete --progress --exclude=.git --exclude=Makefile --exclude='*/default' ./ root@mu07://lustre/usr/modulefiles
